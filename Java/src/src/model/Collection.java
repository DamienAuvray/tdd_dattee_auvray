package src.model;

import java.io.File;
import java.util.ArrayList;

public class Collection {

	/*
	 * Attributes
	 */

	/**
	 * List of document in the collection
	 */
	private ArrayList<File> listDocument;

	/*
	 * Constructors
	 */

	public Collection() {
		listDocument = new ArrayList<>();
	}
	
	@SuppressWarnings("unchecked")
	public Collection(ArrayList<File> list) {
		listDocument = (ArrayList<File>) list.clone();
	}

	/*
	 * Getter & setters
	 */

	/**
	 * get the list of documents
	 */
	public ArrayList<File> getListDocument() {
		return listDocument;
	}
}