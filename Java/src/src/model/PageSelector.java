package src.model;

public enum PageSelector {
	NEXT,
	PREVIOUS,
	FIRST,
	LAST	
}
