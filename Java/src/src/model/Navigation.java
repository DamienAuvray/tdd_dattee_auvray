package src.model;

import java.io.File;

public class Navigation {

	/*
	 * Attributes
	 */

	/**
	 * Collection to navigate
	 */
	private Collection collection;
	/**
	 * current page number
	 */
	private int currentPageNumber;

	/*
	 * Methods
	 */

	/**
	 * Open a document
	 * 
	 * @return file to read
	 * @error return null
	 */
	public File openDocument(File file) {
		try {
			currentPageNumber = Integer.parseInt(file.getName().split(" - ")[0]);
			if(collection.getListDocument().get(currentPageNumber - 1).getPath().equals(file.getPath()))
				return collection.getListDocument().get(currentPageNumber - 1);
			else
				throw new IllegalArgumentException("Format de fichier incorrect");
		} catch (Exception e) {
			throw new IllegalArgumentException("Format de fichier incorrect");
		}
	}

	/**
	 * Change the current page
	 * 
	 * @param page
	 *            : number of the page to load
	 * @return File
	 * @exception IllegalArgumentException
	 */
	public File changePage(int page) {
		if (page < 1 || page > collection.getListDocument().size())
			throw new IllegalArgumentException("Numéro de page incorrect");
		currentPageNumber = page;
		return collection.getListDocument().get(currentPageNumber - 1);
	}

	/**
	 * change the current page
	 * 
	 * @param pageSelector
	 * @return File
	 * @error return null
	 */
	public File changePage(PageSelector pageSelector) {
		switch (pageSelector) {
		case FIRST:
			currentPageNumber = 1;
			return collection.getListDocument().get(0);
		case LAST:
			currentPageNumber = collection.getListDocument().size();
			return collection.getListDocument().get(currentPageNumber - 1);
		case NEXT:
			if (currentPageNumber == collection.getListDocument().size())
				return collection.getListDocument().get(currentPageNumber - 1);
			else {
				currentPageNumber = currentPageNumber + 1;
				return collection.getListDocument().get(currentPageNumber - 1);
			}
		case PREVIOUS:
			if (currentPageNumber == 1)
				return collection.getListDocument().get(currentPageNumber - 1);
			else {
				currentPageNumber = currentPageNumber - 1;
				return collection.getListDocument().get(currentPageNumber - 1);
			}
		default:
			return null;
		}

	}

	/**
	 * Close the navigation set the collection to null
	 */
	public void closeDocument() {
		collection = null;
		currentPageNumber = 0;
	}

	/*
	 * Getters & Setters
	 */

	public Collection getCollection() {
		return collection;
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	public void setCurrentPageNumber(int currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}

	public int getCurrentPageNumber() {
		return currentPageNumber;
	}

}