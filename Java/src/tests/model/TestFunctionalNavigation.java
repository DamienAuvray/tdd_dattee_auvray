package tests.model;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import src.model.Collection;
import src.model.Navigation;
import src.model.PageSelector;

/**
 * 
 * @author administrateur
 * @test Navigation
 *
 * Le dossier de test contient 4 images.
 * Les images sont sous la forme "num page - titre doc"
 * exemple : "2 - Chapitre 1" (2eme page)
 */
public class TestFunctionalNavigation {

	public Navigation navigation;

	@Before
	public void setUp() throws Exception {
		Collection collection = new Collection();
		collection.getListDocument().add(new File("../../../collection/1 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/2 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/3 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/4 - image.jpg"));
		
		navigation = new Navigation();
		navigation.setCollection(collection);
	}

	@After
	public void remove() throws Exception {
		navigation = null;
	}
	
	/**
	 * testFonctionnelSimple()
	 * ouverture de l'image 1 - testFile.jpg
	 * navigation vers la page suivante
	 * navigation vers la 4eme page
	 * navigation vers la premiere page
	 * fermeture du document
	 */
	@Test
	public void testFonctionnelSimple() {
		File file = new File("../../../collection/1 - image.jpg");
		assertEquals(
				"Le document ouvert n'est pas celui selectionné par l'utilisateur",
				file, navigation.openDocument(file));
		assertEquals("Erreur de changement de page",
				1, navigation.getCurrentPageNumber());
		
		navigation.changePage(PageSelector.NEXT);
		assertEquals("Erreur de changement de page",
				2, navigation.getCurrentPageNumber());
		
		navigation.changePage(4);
		assertEquals("Erreur de changement de page",
				4,	navigation.getCurrentPageNumber());
		
		navigation.changePage(PageSelector.FIRST);
		assertEquals("Erreur de changement de page",
				1, navigation.getCurrentPageNumber());
		
		navigation.closeDocument();
		assertNull("Erreur de chargement du document",
				navigation.getCollection());
	}
	
	/**
	 * testFonctionnelSimple2()
	 * ouverture de l'image 1 - testFile.jpg
	 * navigation vers la derniere page
	 * fermeture du document
	 */
	@Test
	public void testFonctionnelSimple2() {
		File file = new File("../../../collection/1 - image.jpg");
		assertEquals(
				"Le document ouvert n'est pas celui selectionné par l'utilisateur",
				file, navigation.openDocument(file));
		assertEquals("Erreur de changement de page",
				1, navigation.getCurrentPageNumber());
		
		navigation.changePage(PageSelector.LAST);
		int lastPageNumber = navigation.getCollection().getListDocument().size();
		assertEquals("Erreur de changement de page",
				lastPageNumber, navigation.getCurrentPageNumber());
		
		navigation.closeDocument();
		assertNull("Erreur de chargement du document",
				navigation.getCollection());
	}
	
	/**
	 * testFonctionnelSimple3()
	 * ouverture de l'image 2 - testFile.jpg
	 * navigation vers la page précédente
	 * fermeture du document
	 */
	@Test
	public void testFonctionnelSimple3() {
		File file = new File("../../../collection/2 - image.jpg");
		assertEquals(
				"Le document ouvert n'est pas celui selectionné par l'utilisateur",
				file, navigation.openDocument(file));
		assertEquals("Erreur de changement de page",
				2, navigation.getCurrentPageNumber());
		
		navigation.changePage(PageSelector.PREVIOUS);
		assertEquals("Erreur de changement de page",
				1, navigation.getCurrentPageNumber());
		
		navigation.closeDocument();
		assertNull("Erreur de chargement du document",
				navigation.getCollection());
	}

}
