package tests.model;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import src.model.*;

/**
 * 
 * @author administrateur
 * @test Navigation
 *
 * Le dossier de test contient 4 images.
 * Les images sont sous la forme "num page - titre doc"
 * exemple : "2 - Chapitre 1" (2eme page)
 */
public class TestUnitNavigation {
	
	public Navigation navigation;

	@Before
	public void setUp() throws Exception {
		Collection collection = new Collection();
		collection.getListDocument().add(new File("../../../collection/1 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/2 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/3 - image.jpg"));
		collection.getListDocument().add(new File("../../../collection/4 - image.jpg"));
		
		navigation = new Navigation();
		navigation.setCollection(collection);
	}
	
	@After
	public void remove() throws Exception {
		navigation = null;
		
	}

	/**
	 * testLoadingFileFail()
	 * 
	 */
	@Test
	public void testLoadingFileFail() {
		try{
			File file = null;
			navigation.openDocument(file);
			fail("Erreur de chargement du document");
		}catch(Exception e){
		}
	}
	
	/**
	 * testLoadingFileUnknownFile()
	 * 
	 */
	@Test
	public void testLoadingFileUnknownFile() {
		try{
			File file = new File("../../../collection/5 - image.jpg");
			navigation.openDocument(file);
			fail("Erreur de chargement du document");
		}catch(Exception e){
		}
	}

	/**
	 * testLoadedFileSuccess()
	 * 
	 */
	@Test
	public void testLoadedFileSuccess() {
		File file = new File("../../../collection/1 - image.jpg");
		assertEquals(
				"Le document ouvert n'est pas celui selectionné par l'utilisateur",
				file, navigation.openDocument(file));
	}
	
	
	/**
	 * testLoadedFileNumberSuccess()
	 * 
	 */
	@Test
	public void testLoadedFileNumberSuccess() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		assertEquals(
				"Le document ouvert n'est pas celui selectionné par l'utilisateur",
				1, navigation.getCurrentPageNumber());
	}

	/**
	 * testFileName()
	 * 
	 */
	@Test
	public void testFileName() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		assertTrue(
				"Le numero de page courrante ne correspond pas à la page ouverte",
				navigation.openDocument(file).getPath()
						.contains("1 - image.jpg"));
	}

	/**
	 * testFileInCollection()
	 * 
	 */
	@Test
	public void testFileInCollection() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		assertTrue("Le document n'existe pas dans la collection", navigation
				.getCollection().getListDocument().contains(file));
	}

	/**
	 * testChangePageNumber()
	 * 
	 */
	@Test
	public void testChangePageNumber() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		navigation.changePage(2);
		assertEquals("Erreur de changement de page",
				2,	navigation.getCurrentPageNumber());
	}

	/**
	 * testChangePagePrevious()
	 * 
	 */
	@Test
	public void testChangePagePrevious() {
		File file = new File("../../../collection/2 - image.jpg");
		navigation.openDocument(file);
		navigation.changePage(PageSelector.PREVIOUS);
		assertEquals("Erreur de changement de page",
				1,
				navigation.getCurrentPageNumber());

	}

	/**
	 * testChangePageNext()
	 * 
	 */
	@Test
	public void testChangePageNext() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		navigation.changePage(PageSelector.NEXT);
		assertEquals("Erreur de changement de page",
				2,
				navigation.getCurrentPageNumber());
	}

	/**
	 * testChangePageFirst()
	 * 
	 */
	@Test
	public void testChangePageFirst() {
		File file = new File("../../../collection/2 - image.jpg");
		navigation.openDocument(file);
		navigation.changePage(PageSelector.FIRST);
		assertEquals("Erreur de changement de page",
				1,
				navigation.getCurrentPageNumber());
	}

	/**
	 * testChangePageLast()
	 * 
	 */
	@Test
	public void testChangePageLast() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		navigation.changePage(PageSelector.LAST);
		int lastPageNumber = navigation.getCollection().getListDocument().size();
		assertEquals("Erreur de changement de page",
				lastPageNumber,
				navigation.getCurrentPageNumber());
	}

	/**
	 * testCloseDocument()
	 * 
	 */
	@Test
	public void testCloseDocument() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		navigation.closeDocument();
		assertNull("Erreur de fermeture du document",
				navigation.getCollection());
	}
	
	/**
	 * testCloseDocumentPageNumber()
	 * 
	 */
	@Test
	public void testCloseDocumentPageNumber() {
		File file = new File("../../../collection/1 - image.jpg");
		navigation.openDocument(file);
		navigation.closeDocument();
		assertEquals("Erreur de fermeture du document",
				0, navigation.getCurrentPageNumber());
	}

}
